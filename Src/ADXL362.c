/********************************************************************************
 Author : CAC (China Applications Support Team) 

 Date :   May, 2014

 File name :  ADXL362.c 

 Description :	 ADXL362 SPI communication driver                

 Hardware plateform : 	EVAL-ADuCM360MKZ and EVAL-ADXL362Z 
 
 Connection:
                 EVAL-ADuCM360MKZ       EVAL-ADXL362Z 
                               
                 P1.4:MISO,             MISO             
                 P1.5:SCLK,             SCLK                                                             
                 P1.6:MOSI,             MOSI
                 P1.7:GPIO as CS,       CS  
********************************************************************************/

#include "stm32l0xx_hal.h"
#include "spi.h"

#include "ADXL362.h"
#include <math.h>
#include "gpio.h"

void delay(int time)
{
   int i;
   for(i=0;i<time;i++)
   ;	
}

/*******************************************************************
  @brief unsigned char ADXL362RegisterRead(unsigned char Address)
         Read a register value from ADXL362         
  @param
         unsigned char Address:       Register address
  @return   
         unsigned int  ReceiveValue:  read register value from ADXL362
*******************************************************************/
unsigned char ADXL362RegisterRead(unsigned char Address)
{   
    unsigned char SendTemp[3];
    unsigned char ReceiveTemp[3];
    unsigned char ReceiveValue;
 
//    DioClr(pADI_GP1,0x80);              //CS down  
    HAL_GPIO_WritePin(SPI_CS_GPIO_Port,SPI_CS_Pin,GPIO_PIN_RESET);
	  SendTemp[0] = 0x0B;                 //0x0B: read register command
    SendTemp[1] = Address;              //address byte
    SendTemp[2] = 0;  
//    SpiFunction(SendTemp, ReceiveTemp, 3);
	  HAL_SPI_TransmitReceive(&hspi1,SendTemp,ReceiveTemp,3,5000);
	
	
    ReceiveValue = ReceiveTemp[2];   
//    DioSet(pADI_GP1,0x80);
	  HAL_GPIO_WritePin(SPI_CS_GPIO_Port,SPI_CS_Pin,GPIO_PIN_SET);//CS up
    return(ReceiveValue);               
}

/*******************************************************************
  @brief void ADXL362RegisterWrite(unsigned char Address, unsigned char SendValue)
         send SPI command to ADXL362
  @param
         unsigned char Address:       Register address
         unsigned char SendValue:     Value written to ADXL362 register
  @return   
         none
*******************************************************************/
void ADXL362RegisterWrite(unsigned char Address, unsigned char SendValue)
{    
    unsigned char SendTemp[3];
    unsigned char ReceiveTemp[3];
    
 //   DioClr(pADI_GP1,0x80);              //CS down
	  HAL_GPIO_WritePin(SPI_CS_GPIO_Port,SPI_CS_Pin,GPIO_PIN_RESET);
	
    SendTemp[0] = 0x0A;                 //0x0A: write register
    SendTemp[1] = Address;              //address byte
    SendTemp[2] = SendValue;
    
 //   SpiFunction(SendTemp, ReceiveTemp, 3);
	  HAL_SPI_TransmitReceive(&hspi1,SendTemp,ReceiveTemp,3,5000);
//    DioSet(pADI_GP1,0x80);              //CS up
	  HAL_GPIO_WritePin(SPI_CS_GPIO_Port,SPI_CS_Pin,GPIO_PIN_SET);
}

/*******************************************************************
  @brief void ADXL362BurstRead(unsigned char Address, unsigned char NumberofRegisters, unsigned char *RegisterData)
         Multibyte read from ADXL362
  @param
         unsigned char Address:           Register address
         unsigned char NumberofRegisters: Register numbers to be read
         unsigned char *RegisterData:     Buffer save the read value
  @return   
         none  
*******************************************************************/
void ADXL362BurstRead(unsigned char Address, unsigned char NumberofRegisters, unsigned char *RegisterData)
{    
    unsigned char SendTemp[2];
    unsigned char ReceiveTemp[2];
    unsigned char RegisterIndex;
  
//    DioClr(pADI_GP1,0x80);         //CS down
	  HAL_GPIO_WritePin(SPI_CS_GPIO_Port,SPI_CS_Pin,GPIO_PIN_RESET);
    SendTemp[0] = 0x0B;            //0x0B: read register  
    SendTemp[1] = Address;         //address byte  
 //   SpiFunction(SendTemp, ReceiveTemp, 2);
	  HAL_SPI_TransmitReceive(&hspi1,SendTemp,ReceiveTemp,2,5000);
//    SpiFifoFlush(pADI_SPI0,SPICON_TFLUSH_EN,SPICON_RFLUSH_EN);
	
  
    SendTemp[0] = 0x00;
    for (RegisterIndex=0; RegisterIndex<NumberofRegisters; RegisterIndex++)
    {    
//        SpiFifoFlush(pADI_SPI0,SPICON_TFLUSH_EN,SPICON_RFLUSH_EN);
//        SpiFunction(SendTemp, ReceiveTemp, 1);
			  HAL_SPI_TransmitReceive(&hspi1,SendTemp,ReceiveTemp,1,5000);
        *(RegisterData + RegisterIndex) = ReceiveTemp[0];        
    }
//    DioSet(pADI_GP1,0x80);         //CS up
		HAL_GPIO_WritePin(SPI_CS_GPIO_Port,SPI_CS_Pin,GPIO_PIN_SET);
}

/*******************************************************************
  @brief void ADXL362BurstWrite(unsigned char Address, unsigned char NumberofRegisters, unsigned char *RegisterData)
         Multibyte write to ADXL362
  @param
         unsigned char Address:           Register address
         unsigned char NumberofRegisters: Register numbers to be written
         unsigned char *RegisterData:     Buffer save the written value
  @return   
         none 
*******************************************************************/
void ADXL362BurstWrite(unsigned char Address, unsigned char NumberofRegisters, unsigned char *RegisterData)
{ 
    unsigned char SendTemp[2];
    unsigned char ReceiveTemp[2];
    unsigned char RegisterIndex;
  
//    DioClr(pADI_GP1,0x80);              //CS down
	  HAL_GPIO_WritePin(SPI_CS_GPIO_Port,SPI_CS_Pin,GPIO_PIN_RESET);
    SendTemp[0] = 0x0A;                 //0x0A: write register
    SendTemp[1] = Address;              //address byte  
//    SpiFunction(SendTemp, ReceiveTemp, 2);
	  HAL_SPI_TransmitReceive(&hspi1,SendTemp,ReceiveTemp,2,5000);

    for (RegisterIndex=0; RegisterIndex<NumberofRegisters; RegisterIndex++)
    {    
        SendTemp[0] = *(RegisterData + RegisterIndex);
//        SpiFunction(SendTemp, ReceiveTemp, 1);       
			HAL_SPI_TransmitReceive(&hspi1,SendTemp,ReceiveTemp,1,5000);
    }    
//    DioSet(pADI_GP1,0x80);              //CS up
    HAL_GPIO_WritePin(SPI_CS_GPIO_Port,SPI_CS_Pin,GPIO_PIN_SET);
}

uint16_t ADXL362_Acc_Read_12Bit(unsigned char Low_Address)
{
	 uint8_t Data[2];
	 uint16_t Result;
   Data[0] = ADXL362RegisterRead(Low_Address);
	 Data[1] = ADXL362RegisterRead(Low_Address+1);
	 Result = (Data[1]&0x0f)*0xff+Data[0];
	 return Result;
}

//通过三轴加速度值计算倾角.反余弦查表法.
uint16_t Angle_Calculation()
{
//	 uint8_t Acc_x,Acc_y,Acc_z;
	 double sum_sqrt;
//	 uint8_t Acc_x_ABS,Acc_y_ABS,Acc_z_ABS;
	 double value_cos;
	 int sum;
	 double sum_double;
	 int angle;
	 uint16_t Acc_x_adjust,Acc_y_adjust,Acc_z_adjust;
	 uint16_t Acc_x_12bit,Acc_y_12bit,Acc_z_12bit;
	
	
#if READING_8BIT_VALUE	
	
   Acc_x = ADXL362RegisterRead(XL362_XDATA);
//	 delay(1000);
	 Acc_y = ADXL362RegisterRead(XL362_YDATA);
//	 delay(1000);
	 Acc_z = ADXL362RegisterRead(XL362_ZDATA);
//	 printf("x: %d,y: %d,z: %d\n",Acc_x,Acc_y,Acc_z);
	
	 if(Acc_x>0x80) Acc_x_adjust = 0xff - Acc_x ;
   else Acc_x_adjust = Acc_x;	
	 if(Acc_y>0x80) Acc_y_adjust = 0xff - Acc_y ;
   else Acc_y_adjust = Acc_y;	
	 if(Acc_z>0x80) Acc_z_adjust = 0xff - Acc_z ;
   else Acc_z_adjust = Acc_z;		 
	 
	 printf("x_adjust: %d.y_adjust: %d.z_adjust: %d.\n",Acc_x_adjust,Acc_y_adjust,Acc_z_adjust);

#endif

#if READING_12BIT_VALUE

   Acc_x_12bit = ADXL362_Acc_Read_12Bit(XL362_XDATA_L);
   Acc_y_12bit = ADXL362_Acc_Read_12Bit(XL362_YDATA_L);
   Acc_z_12bit = ADXL362_Acc_Read_12Bit(XL362_ZDATA_L);
//   printf("Acc_x_12bit: %d.Acc_y_12bit: %d.Acc_z_12bit: %d.\n",Acc_x_12bit,Acc_y_12bit,Acc_z_12bit); 
	 
	 if(Acc_x_12bit>0x800) Acc_x_adjust = 0xfff - Acc_x_12bit ;
   else Acc_x_adjust = Acc_x_12bit;	
	 if(Acc_y_12bit>0x800) Acc_y_adjust = 0xfff - Acc_y_12bit ;
   else Acc_y_adjust = Acc_y_12bit;	
	 if(Acc_z_12bit>0x800) Acc_z_adjust = 0xfff - Acc_z_12bit ;
   else Acc_z_adjust = Acc_z_12bit;		
 

#endif
	 
//	 Acc_x_ABS = Acc_x & 0x7F; //转化为绝对值;
//	 Acc_y_ABS = Acc_y & 0x7F;
//	 Acc_z_ABS = Acc_z & 0x7F;
//	 printf("Acc_x_ABS=%d,Acc_y_ABS=%d,Acc_z_ABS=%d.\n",Acc_x_ABS,Acc_y_ABS,Acc_z_ABS);
	
//	 sum=Acc_x_ABS * Acc_x_ABS + Acc_y_ABS * Acc_y_ABS + Acc_z_ABS * Acc_z_ABS;
     sum=Acc_x_adjust * Acc_x_adjust + Acc_y_adjust * Acc_y_adjust + Acc_z_adjust * Acc_z_adjust;
//	 printf("sum=%d.\n",sum);
	 
	 sum_double= sum *1.000000;
//	 printf("sum_double=%lf.\n",sum_double);
	 
	 sum_sqrt = sqrt(sum_double);
//	 printf("sum_sqrt=%lf.\n",sum_sqrt);
	 value_cos = Acc_z_adjust/sum_sqrt;
//	 printf("cos:%lf.\n",value_cos);
	 
	 angle = Angle_Cos_Reverse(value_cos);
	 
#if READING_8BIT_VALUE	
if(Acc_z < 0x80) angle = 180-angle;
#endif

#if READING_12BIT_VALUE
//	 if(Acc_z_12bit < 0x800) angle = 180-angle;
     if(Acc_z_12bit > 0x800) angle = 180-angle;
#endif

//   angle = 180-angle;
	 
	 printf("angle:%d.\n",angle);
	 
	 return angle;
}

//反余弦计算角度;
uint32_t Angle_Cos_Reverse(double value)
{
	 uint32_t result_angle;
	 double comparing_value[91]={
1,
0.999847695,
0.999390827,
0.998629535,
0.99756405,
0.996194698,
0.994521895,
0.992546152,
0.990268069,
0.987688341,
0.984807753,
0.981627183,
0.978147601,
0.974370065,
0.970295726,
0.965925826,
0.961261696,
0.956304756,
0.951056516,
0.945518576,
0.939692621,
0.933580426,
0.927183855,
0.920504853,
0.913545458,
0.906307787,
0.898794046,
0.891006524,
0.882947593,
0.874619707,
0.866025404,
0.857167301,
0.848048096,
0.838670568,
0.829037573,
0.819152044,
0.809016994,
0.79863551,
0.788010754,
0.777145961,
0.766044443,
0.75470958,
0.743144825,
0.731353702,
0.7193398,
0.707106781,
0.69465837,
0.68199836,
0.669130606,
0.656059029,
0.64278761,
0.629320391,
0.615661475,
0.601815023,
0.587785252,
0.573576436,
0.559192903,
0.544639035,
0.529919264,
0.515038075,
0.5,
0.48480962,
0.469471563,
0.4539905,
0.438371147,
0.422618262,
0.406736643,
0.390731128,
0.374606593,
0.35836795,
0.342020143,
0.325568154,
0.325568154,
0.292371705,
0.275637356,
0.258819045,
0.241921896,
0.224951054,
0.207911691,
0.190808995,
0.190808995,
0.156434465,
0.139173101,
0.121869343,
0.104528463,
0.087155743,
0.069756474,
0.052335956,
0.034899497,
0.017452406,
0
};
   for(result_angle=0;result_angle<90;result_angle++)
   {
     if(value<=comparing_value[result_angle]&&value>comparing_value[result_angle+1])
		 break;
   } 
   return result_angle;

}


//adxl362测试.若返回0xAD.则表示通信成功;
void Adxl362_test()
{
   uint8_t reading;
	 reading = ADXL362RegisterRead(0x00);
	 printf("adxl362 ID:%x\n",reading);
	
}

//初始参数设置.
void Adxl362_initial_setting()
{
	 ADXL362RegisterWrite(XL362_THRESH_ACT_L,(int)(ACT_THRESHOLD*1024)%0xff);//1024 = g,若量程为2g,则g=1024,0x0400,100 0000 0000
	 ADXL362RegisterWrite(XL362_THRESH_ACT_H,(int)(ACT_THRESHOLD*1024)/0xff);//设置运动阀值
	
	 ADXL362RegisterWrite(XL362_TIME_ACT,(int)(HOLD_TIME_ALARM*12.5));//设置运动阀值持续时间.25/12.5=2 S.//只有测量模式下有效.唤醒模式下无效.
	 ADXL362RegisterWrite(XL362_THRESH_INACT_L,102);//设置静止阀值为0.1g.意思是在指定时间内加速度位移低于0.1g,则判断为进入静止状态.
	 ADXL362RegisterWrite(XL362_THRESH_INACT_H,0x00);
	 ADXL362RegisterWrite(XL362_TIME_INACT_L,25);//设置静止阀值时间为2秒.12.5x2=25
	 ADXL362RegisterWrite(XL362_TIME_INACT_H,0x00); 
	 ADXL362RegisterWrite(XL362_ACT_INACT_CTL,0x3F);//检测模式为环路模式,使能运动和静止检测并都设置 为相对检测而非绝对检测.
	 ADXL362RegisterWrite(XL362_FIFO_CONTROL,0x00);//FIFO功能暂不使用.
	 ADXL362RegisterWrite(XL362_FIFO_SAMPLES,0x80);//FIFO功能暂不使用.
	 ADXL362RegisterWrite(XL362_INTMAP1,0x20);//INT1设置为运动唤醒，输出高电平.
	 ADXL362RegisterWrite(XL362_INTMAP2,0x10);//INT2设置为静止触发，输出高电平.
//	 ADXL362RegisterWrite(XL362_POWER_CTL,0x0E);//设置进入测量模式.工作在唤醒模式.使能自动休眠. 运动阀值持续时间无效.功耗更低.
	 ADXL362RegisterWrite(XL362_POWER_CTL,0x02);//设置进入测量模式.工作在非唤醒模式.不使能自动休眠.
	 ADXL362RegisterWrite(XL362_FILTER_CTL,0x10);//选择量程为+-2g,采样频率(ODR)为12.5hz.	
	
}




