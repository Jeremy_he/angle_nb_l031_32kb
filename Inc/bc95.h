#ifndef __BC95_H
#define __BC95_H

#include "usart.h"
#include "stdio.h"
#include "string.h"


//#define SENDING_DATA_TYPE_UDP
//#define SENDING_DATA_TYPE_COAP

#define BC95_PWR_ON_TEST

//默认频段
#define SET_UE_DEFAULT_BAND      "AT+NBAND=5"         
#define UE_DEFAULT_BAND          "+NBAND:5"


#define SET_UE_REBOOT            "AT+NRB"

#define QUERY_UE_BAND            "AT+NBAND?"
#define SET_UE_BAND_5            "AT+NBAND=5"
#define SET_UE_BAND_8            "AT+NBAND=8"

#define QUERY_UE_CONNECT_MODE    "AT+NCONFIG?"
#define SET_UE_AUTOCONNECT       "AT+NCONFIG=AUTOCONNECT,TRUE"
#define SET_UE_MANUALCONNECT     "AT+NCONFIG=AUTOCONNECT,FALSE"
#define SET_AUTO_CONNECT_TRUE   "AUTOCONNECT,TRUE"

#define QUERY_UE_FUNC            "AT+CFUN?"
#define SET_UE_FUNC_0            "AT+CFUN=0"
#define SET_UE_FUNC_1            "AT+CFUN=1"

#define QUERY_UE_SIGNAL_QTY      "AT+CSQ"

#define QUERY_UE_ATTACH_STATS    "AT+CGATT?"
#define UE_ATTACHED_STATS        "+CGATT:1"
#define SET_UE_ATTACH            "AT+CGATT=1"

#define QUERY_UE_EREG_STATS      "AT+CEREG?"
#define UE_EREGISTERING_STATS    "+CEREG:0,2"
#define UE_EREGISTERED_STATS     "+CEREG:0,1"
#define UE_EREGISTERED_OK "1" //实测有时会返回+CEREG:1,1.也定义为注网成功.
#define SET_UE_EREG              "AT+CEREG=1"

#define QUERY_UE_SCCON_STATS     "AT+CSCON?"
#define SET_UE_SCCON             "AT+CSCON=1"

#define QUERY_CDP_SERVER_IP_PORT "AT+NCDP?"
#define SET_CDP_SERVER_IP_PORT  "AT+NCDP=122.112.245.34,5683"
#define UE_DEFAULT_CDP_SERVER_IP_PORT  "+NCDP:122.112.245.34,5683"

#define QUERY_IMEI_NUMBER "AT+CGSN=1"
#define QUERY_IMEI_NUMBER_ANSWER "+CGSN:"

#define SET_MESSAGE_INDICATION "AT+NSMI=1"
//实测每次发送消息前一定要执行这一条.

#define SET_MESSAGE_DOWN_STREAM "AT+NNMI=2"

#define CREATE_UDP_SOCKET "AT+NSOCR=DGRAM,17,3005,1"
//#define SEND_UDP_DATA "AT+NSOST=0,122.112.245.34,5683,30,420300116f43b46d7174740467616d6549633d636f6170353637ff3132"//topic=game "67616d65"
//#define SEND_UDP_DATA "AT+NSOST=0,122.112.245.34,5683,41,420300116f43b46d7174740411111111111111111111111111111149633d636f6170353637ff3132"
//topic= "111111111111111111111111111111".预留30个字节用作IMEI号做主题
#define SEND_UDP_DATA "AT+NSOST=0,122.112.245.34,5683,32,420300116f43b46d717474041111111149633d636f6170353637ff4a44"
//topic="11111111" IMEI后4个字节.

#define CLOSE_UDP_SOCKET "AT+NSOCL=0"
#define TOPIC_START_BYTE 58


typedef struct
{
	char manufacture_id[12];
	char device_module[18];
	char firmware_version[30];
	char frequency_band[10];
} BC95_UE_INFO_typedef;


void delay_ms(int ms);
void BC95_power_on(void);
uint8_t* BC95_check_ack(char *str);
uint8_t  BC95_send_cmd(char *cmd,char *ack,uint16_t waittime);
uint8_t creat_UDP_socket(char* local_port);
uint8_t  send_UDP_msg(char *socket,char *hostIP,char *port,char *dataLen,char *data);
char* receive_udp(char *socket,char *dataLen);
void get_str_data(char* des,char pos,char len);
uint8_t query_net_status(void);
void BC95_Test_Demo(void);
//void send_COAP_message(uint8_t data);
void send_UDP_data(uint8_t data);
void Query_IMEI_number(void);

#endif

